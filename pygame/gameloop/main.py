import pygame
from pygame import *
from pygame import freetype

from pygame.locals import (
    K_UP,
    K_DOWN,
    K_LEFT,
    K_RIGHT,
    K_ESCAPE,
    KEYDOWN,
    QUIT,
)

class Simulation():
    def __init__ (self, width=1920, height=1080):
        self.clock = pygame.time.Clock()
        self.running = False
        self.screen_width = width
        self.screen_height = height
        pygame.init()
        # Set up the drawing window
        self.screen = pygame.display.set_mode(
            (0, 0), pygame.HWSURFACE | pygame.FULLSCREEN)
        #screen = pygame.display.set_mode((1366,768),pygame.FULLSCREEN)
        self.screen.fill((0, 0, 0))

    def run(self):
        self.running = True
        while self.running:
            for event in pygame.event.get():
                if event.type == KEYDOWN:
                    if event.key == K_ESCAPE:
                        self.running = False
                    elif event.key == K_DOWN:
                        pass
                    elif event.key == K_UP:
                        pass
                    elif event.key == K_LEFT:
                        pass
                    elif event.key == K_RIGHT:
                        pass
                elif event.type == pygame.QUIT:
                    self.running = False
            self.clock.tick(60)
            pygame.display.flip()
        pygame.quit()

if __name__ == "__main__":
    simulation = Simulation()
    simulation.run()