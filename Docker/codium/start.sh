#!/bin/bash
docker run -it --rm \
  --mount "type=bind,src=$(pwd)/shared,dst=/home/user" \
  --workdir /home/user \
  -e DISPLAY=$DISPLAY \
  -v /tmp/.X11-unix:/tmp/.X11-unix \
  codium:0.0.1 \
  /bin/bash
